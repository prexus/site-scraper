# Metrics

The metrics endpoint offers the basic nodejs metrics. You can use predefined Grafana dashboards to avoid configuring everything again. Check out dashboard [#11159](https://grafana.com/grafana/dashboards/11159) or [#11956](https://grafana.com/grafana/dashboards/11956) and use the import feature in grafana. If you are working in an offline environment you can download the JSON, put it on a USB and import it manually at your location.

There are additional metrics that can be added to your dashboard. Import the [grafana-dashboard.json](../docker/grafana-dashboard.json) to your Grafana instance for inspiration.

<div align="center">
    <br>
    <a href="grafana_full.png">
        <img src="grafana_small.png" title="Grafana showing a running instance of the scraper">
   </a><br>
   <small>Grafana showing a running instance of the scraper</small>
   <br>
   <br>
   <br>
</div>

See the [docker-folder](../docker/) for inspiration for your Loki/Prometheus/Grafana stack. A [docker-compose](../docker/docker-compose.yml) is supplied that should get you up and running pretty quicly.

The endpoint for getting the metrics can be configured using environment variables - see the [configuration](./CONFIGURATION.md) document for more details.

## Listings metrics

#### `scraper_added_listings`

How many listings has been added

## Scrape metrics

#### `scraper_pending_scrapes`

How many scrapes are waiting to start

#### `scraper_completed_listings`

How many scrapes has completed

#### `scraper_failed_listings`

How many scrapes has failed

## Captcha metrics

#### `scraper_detected_captchas`

How many captcha needs to be solved

#### `scraper_solved_captchas`

How many captcha was successfully solved

#### `scraper_failed_captchas`

How many captcha failed solving

