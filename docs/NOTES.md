# Notes
Here you will find some notes and reminders on things I ran into going through the process of developing this scraper.

## Package version
- `file-type` is v16 because 17+ is ESM only.

## Important reminders for workers

### Workers run in seperate threads
Workers run in the own thread so think of them as completely seperate node processes also when it comes to imports, db connections, etc.

### Using @paths
Workers needs to `import ./alias` to be able to use the ts imports (eg. '@services/logger.service').

### Worker metrics
Since workers cannot report back to the main thread (because observers on threadsjs doesnt like async...) it is using a metrics helper to make it store metrics in the database. The main thread will read these every second and record them accordingly. It's a bit of a hack, but the only solution as I see it now.

