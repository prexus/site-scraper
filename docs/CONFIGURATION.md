# Configuration

A list of environment variables are available to configure the scraper.

## Environment variables

### Paths

#### `DATA_PATH`
- Optional
- Default: `data`
- Example: `/opt/data`

Where database, scrapes and exports are stored by default.

#### `SCRAPES_PATH`
- Optional
- Default: `scrapes`
- Example: `my-scrapes`

Folder relative to the `DATA_PATH` where scrapes are stored before they are exported.

#### `EXPORTS_PATH`
- Optional
- Default: `exports`
- Example: `my-exports`

Folder relative to the `DATA_PATH` where exports are stored after a scrape is completed.

#### `LOGS_PATH`
- Optional
- Default: `logs`
- Example: `my-logs`

Folder relative to the `DATA_PATH` where logs are stored.

### Configuration files

#### `PROXIES_FILE`
- Optional
- Default: `proxies.json`
- Example: `proxies-eu.json`

Path to the proxy configuration file relative to the `DATA_PATH`.

#### `CREDENTIALS_FILE`
- Optional
- Default: `credentials.json`
- Example: `verified-users.json`

Path to the credentials file relative to the `DATA_PATH`.

### Listing worker

#### `LISTING_WORKER`
- Optional
- Default: `1`
- Example: `0`

Used for disabling the listing worker.

#### `LISTING_WORKER_RESCAN`
- Optional
- Default: `0`
- Example: `1`

When set to `1` the listing worker will scan all the available pages to check if it missed anything.

#### `LISTING_WORKER_MAX_PAGE`
- Optional
- Default: `0`
- Example: `3`

Defines the maximum number of pages read for listings.

### Scrape workers

#### `SCRAPE_WORKERS`
- Optional
- Default: `0`
- Example: `12`

The number of scrape workers spawned as the system starts.

### Logging

#### `LOG_LEVEL`
- Optional
- Default: `debug`
- Example: `warn`

Sets the log level accordingly.

#### `LOG_LOKI`
- Optional
- Default: _none_
- Example: `http://loki:3100`

The URL to a running Loki instance where logs will be forward to.

### Web server

#### `WEB_SERVER_PORT`
- Optional
- Default: `8080`
- Example: `9090`

Defines what port the web server is exposed on. This defines where metrics are exposed from.

### Required APIs

#### `API_KEY_2CAPTCHA`
- Required
- Example: `5ab6fbe8b3d17fabead8f3a28d7d8579`

Defines what port the web server is exposed on. This defines where metrics are exposed from.

### Metrics

#### `METRICS_PATH`
- Optional
- Default: `/metrics`
- Example: `/prometheus`

Sets the path to the metrics where Prometheus can fetch data.

## Credentials

To be able to retrieve the largest amount of phone numbers from the listings you will need to provide the users that can be used for login. Verified users works the best. You can find an example of the required configuration.json in the ./data folder, but the structure is super simple:

```json
[
    {
        "email": "my@email.com",
        "password": "supErSecrETpasSw0RD!"
    }
    // ...
]
```

Be aware that the provided users could get banned if the site doesn't like what you are doing with them.

## Proxies

The system uses proxies to spread out over multiple IP addresses to avoid detection. Provide it with a healthy list of proxies to use. Don't worry about providing it slow proxies, if it deems them too slow it will use a different one.

```json
[
    {
        "url": "http://my-proxy.com:8080",
        "username": "user",
        "password": "password",
        "destination": "Greenland, Nuuk"
    }
    // ...
]
```
Note: Destination is only used for logging purposes to be able to refer it back to the used proxy in case it is consistently proving to be slow or otherwise produce failing scrapes.

You can find free proxies online or you can use paid services like [PrivateVPN](https://privatevpn.com) (see their [proxy list](https://privatevpn.com/serverlist/) available for paid users)
