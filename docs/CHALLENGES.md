# Trial and error

Of course, the site owners is interested in protecting their data so I have run my head against a lot of walls in this project. Some things I have solved, and others I have had to circumvent or just give up on. Here are some of the things that I have run into:

## Imperva / ReCAPTCHA
The first thing I discovered was that the site I was scraping is protected by Imperva. If they see a suspecious request they will throw you a page with a ReCAPTCHA V2 challenge that needs to be solved. To circumvent this, I'm using [2Captcha](https://2captcha.com/) who offers solving these extremely cheap, paying just ~4 USD to solve 1,500 requests.

I decided to rework the workers a bit once this was in place so the browser would keep the 'verified' state as it was both a lot faster (solving the challenge can take 30-60 seconds) and cheaper in the long run. So no more closing and opening the browser - which I guess makes good sense not to do anyways.

On some occations when going at the APIs directly, they will also return a ReCAPTCHA challenge which honestly seem like a bug. It would make more sence to return a status code 401 or 403 instead of a HTML document with a challenge. But I guess this is not really an issue as real users would never run into this issue.

## Data hidden behind verified users
Some profiles has a phone number and to get that on _classified_ and _community_ listings you need to be logged in with a verified user. This requires a dedicated phone number per account you use, so that's kind of tricky. I do not have access to unlimited phone numbers, so that is a hard one to work around.

## 403 Random profiles (no way to detect)
When you scraping data you want the server to see you as a real user - and triggering errors is never good because you might be flagged. When accessing the profile endpoint you will sometimes get a status code 403 back (forbidden). I have not been able to figure out why I get it on some profiles and not others. Generally speaking if the profile has a phone number it will not happen - but if they don't, they might return the rest of the data (like creation time, full name, etc) - or they might give a 403. Puzzling.

## User handling
### Users being banned
After having everything running pretty good for a few weeks I suddently saw a spike in error messages in Grafana. After investigating I discovered that most of my users had been banned. They now got a status code 403 when logging in and the server responded with a message that the account had been blocked.

### Rate limiting
Seeing my users being targeted by the ban hammer, I wanted to try to use a single user because it was a lot more affordable to lose one user at a time instead of one per running worker. Unfortunately after giving the credentials handling an overhaul I ran into another problem - rate limiting. I could see the problem right away when 12 workers started up across 12 different countries they started getting hit with status code 429 from the server (Too many requests).

### Solving user issues is hard
The solution to my user woes would be creating users automatically and have a buffer build up as they are getting taken down. The endpoint for creating users is easy to access and using a domain with a catch-all e-mail account for account verification would make me able to create the users.

The only thing that is not solved is all the phone numbers required for properly verified user accounts - and I don't think there is a reasonable solution to that issue.

## Distributed workers
Originally I designed the system with the thought of using distributed workers to do the parsing. After a lot of testing and optimizing I discovered that a single computer could easily run up to 16 workers at the same time and it was plenty to keep up with the stream of incoming listings. Using proxies I can easily hide the fact that I was running everything on the same computer.
