# Misc
 - Figure out what to do with helpers vs services. What is what? And why! It's a bit confusing.
   Page service is a great example, it's responsibility is worker stuff but it's a service.
 - Make logging easier indexed by moving outputter to log fields (or label for console logger)
 - Add credentials pool like loggers to make it flicker a bit more all over the place.
     - Note if a user is 'verified'
     - Use verified users (if available) on categories where requiresVerifiedUser is true (eg classified and community)
 - Interesting things to add to grafana
    - Disk usage
    - Data throughput
    - Disk saved (zipping)

# CLEANUP
 - Create demo credentials
 - Create demo proxies
 - Clean repo history when all data is clean
