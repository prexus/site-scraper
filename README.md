# Site Scraper
## About
This project is an experiment to see what kind of challenges you would face when trying to index or scrape the data on a widely used classified website. I expected to be faced with all kind of deterrents and surely enough, it should turn out to be quite an experience!

In short, the system scrapes all listings in multiple categories and then screenshots and extracts whatever data is publicly available. It then organizes the data and exports a `.scrape` file containing the meta data for each individual listing.

It is not doing anything that your browser isn't already doing - the only difference is that the system can orcestrate as many browsers as your CPU and RAM can handle.

<div align="center">
    <br>
    <a href="./docs/grafana_full.png">
        <img src="./docs/grafana_small.png" title="Grafana showing a running instance of the scraper">
   </a><br>
   <small>Grafana showing a running instance of the scraper</small>
   <br>
   <br>
   <br>
</div>

## Is scraping legal?
If scraping websites was illegal we would fairly quickly see Google go out of business. Their whole business model started out relying on scraping websites for content, indexing it and make it searchable through their website.

As web developers we even design `robot.txt` files to tell bots how we prefer our sites to be read including site-maps to help it navigate the site.

U.S. courts have reaffirmed that scraping is legal and you can read more about it in [this article](https://techcrunch.com/2022/04/18/web-scraping-legal-court/) by [Zack Whittaker](https://twitter.com/zackwhittaker).

You should never scrape a website so it affects the use of the site to other users. If you scrape agressively with hundreds of scrapers you could easily kill sites hosted on smaller systems.

Don't do that. Scrape morally! :)

# Data extraction and manipulation
The exercise is to extract and manipulate the data sot it can be indexed and made searchable. The scrape worker will collect the following:

- Data from listing worker
- Screenshot of the full page
- Page source
- Profile data

## Results
From the sources we can extract quite a few things which can be indexed and made searchable.

We already have the screenshot ready, and things like the original listing data, the profile source data and the page source is also included in the .scrape file as we it is good to have in case we discover that we are missing some data points.

<div align="center">
    <br>
    <a href="./docs/scrape_full.png">
        <img src="./docs/scrape_small.png" title="The content of a .scrape file">
   </a><br>
   <small>The .scrape file contains all the data that was collected</small>
   <br>
   <br>
   <br>
</div>

#### `extracts.json`
This artifact is the outcome of the extract helper. It contains basic information gathered from the listing, profile and source:

- A context object used to populate the UI
- Data of the user creating the listing
- The primary phone number
- Inline phone numbers
- Listing description
- Photo URLs

This extract is used to further populate the meta.json but it contains much more information, albeit less structured, than the meta.json does.

#### `photo-n.ext`
From the extracts.json the photo URLs are used to download the actual files which are then included in the .scrape file.

#### `meta.json`
This file is your entrypoint after getting the .scrape file loaded into your system. It contains an easily navigatable structure with data about the scrape itself, the page, the user and the included photos and data files.

For a quick peak of what could be find in the meta.json check out the [interface definition](src/interfaces/meta.ts).

# What is the use of the data
So now we have a bunch of data, what can we use it for?

Well, start by using the meta.json to index the data in the database of your choice. Now you can either build your own software or use existing tools on top of your dataset. I have tried to come up with a few examples on how to utilize the data:
<br><br>
<table>
    <tr>
        <td width="50%" valign="top">
            <b>Statistics</b><br>
            Create a database of car brands, the models and the sales prices over time. Eventhough the meta.json does not contain specifics about the listings (eg. car model, price, km, etc) this data is still available in the extract.json in the form of the context object.
        </td>
        <td width="50%" valign="top">
            <b>Analytics</b><br>
            Use the price develpment of the market to assist you in making predictions about the future. You can also use it to see what users has the most items for sale or what is the current inventory of real estate agents.
        </td>
    </tr>
    <tr>
        <td width="50%" valign="top">
            <b>History</b><br>
            You will be able to find listings that are no longer available. You can identify the user behind the listing and, if the user provided the information, see the users phone number. You are also able to see if that user currently have listings up on the site and what they have listed earlier.
        </td>
        <td width="50%" valign="top">
            <b>Alterting</b><br>
            You can setup certain keywords to produce an alert in case a user sells illegal items. You can also keep track of specific users in case they list new items.
        </td>
    </tr>
    <tr>
        <td width="50%" valign="top">
            <b>Image recognition</b><br>
            Using the steady stream of incoming images you can pipe them through image recoginition software to inform you in case they contains faces. These faces can then be correlated with faces found on sites like Interpol or maybe just your personal photo in case someone took a picture of you by accident.
        </td>
        <td></td>
    </tr>
</table>
<br>

# How about generic scrapers?
There are plenty of scrapers out there that could be used to traverse a website. The issue is that they don't give you the ability to extract the data for easy indexing the same way that this project does. This custom scraper knows its way around the site's different quirk and that makes it able to get specific details that is not possible in generic scrapers.

I also learned that not all of them handles reCAPTCHAs properly - and some not at all - which make them useless in this case.

It comes down to what gives you the most streamlined and least maintainable way of scraping all the data that you need.

## Can you use this to scrape other targets?
By default, no. This project is made to scrape a specific site and extract information using customized triggers via the DOM interacting with the page as a real user would and doing tailored API calls.

You could expand on the project to allow for collectors (like the listing workers) and adaptors (like the scrape worker) so you could support scraping of multiple sites - because technically the worker system is fairly flexible. It's not something I have put work into as it was not relevant for this test.

# Running the project

### Starting the scraper
Before you start the project be sure to checkout the [configuration](./docs/CONFIGURATION.md) as a list of proxies and credentials needs to be provided!

The project starts using [nodemon](https://nodemon.io) for easier development and a sensible configuration is already provided for you via `nodemon.json`.

```bash
$ npm start
```

The default log level is set to `debug` and by default you start with the listing worker and only one scrape worker.

### Linting
The code can be linted using airbnb's typescript settings plus a few modifications of my own.

```bash
$ npm run lint
```

# More documentation

### System setup
- [Configuration](./docs/CONFIGURATION.md)
- [Metrics](./docs/METRICS.md)
- [Known issues](./docs/KNOWN_ISSUES.md)

### Wiki
- [Challenges](./docs/CHALLENGES.md)
- [Notes](./docs/NOTES.md)
- [Todo](./docs/TODO.md)
