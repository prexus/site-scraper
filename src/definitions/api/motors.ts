import { Category } from '@enums/category';
import { SiteScraperAPI } from '@interfaces/site-scraper-api';

const motors: SiteScraperAPI = {
    type: Category.MOTORS,
    index: 'motors.com',
    apiSource: 'https://site-scraper.com/motors/',
    filters: '("site.id":"2") AND ("category_v2.slug_paths":"motors") AND (promoted:false)',
    attributes: [
        'promoted',
        'is_premium',
        'is_featured_agent',
        'location_list',
        'objectID',
        'name',
        'price',
        'neighbourhood',
        'agent_logo',
        'can_chat',
        'details',
        'photo_thumbnails',
        'highlighted_ad',
        'absolute_url',
        'id',
        'category_id',
        'uuid',
        'category',
        'has_phone_number',
        'category_v2',
        'photos_count',
        'description',
        'created_at',
        'site',
        'has_vin',
        'auto_agent_id',
        'is_trusted_seller',
        'show_trusted_seller_logo',
        'trusted_seller_logo',
        'trusted_seller_id',
        'created_at',
        'added',
        'jobs_logo',
        'vas',
        'seller_type',
        'has_video'
    ]
};

export default motors;
