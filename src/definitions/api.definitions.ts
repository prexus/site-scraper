import classified from './api/classified';
import community from './api/community';
import motors from './api/motors';
import propertyForRentResidential from './api/property-for-rent-residential';

export default {
    classified,
    community,
    motors,
    propertyForRentResidential
};
