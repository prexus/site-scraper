import { Category } from '@enums/category';

export interface Meta {
    scrape: ScrapeMeta;
    page: Partial<PageMeta>;
    user: Partial<UserMeta>;
    files: Partial<FilesMeta>;
}

export interface ScrapeMeta {
    version: string;
    created: number;
    started: number;
    completed: number;
    category: Category;
    identifier: string;
}

export interface UserMeta {
    id: string;
    companyId: string;
    created: number;
    name: string;
    company: string;
    photo: string;
    phoneNumber: string;
    otherPhoneNumbers: string[];
}

export interface PageMeta {
    created: number;
    added: number;
    url: string;
    headline: string;
    content: string;
    photos: string[];
}

export interface FilesMeta {
    meta: string;
    source: string;
    screenshot: string;
    [key: string]: string | string[];
}
