export interface Metric {
    id: number;
    created: number;
    type: string;
    method: string;
    data: string;
}
