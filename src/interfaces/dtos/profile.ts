export interface ProfileReponse {
    phone_number: string;
    is_agent: boolean;
    date_joined: string;
    full_name: string;
    photo_url: string;
}
