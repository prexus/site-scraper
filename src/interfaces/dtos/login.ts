export interface LoginTokenResponse extends TokenResponse { }

export interface RefreshTokenResponse extends TokenResponse {
    errors?: string[];
}

export interface TokenResponse {
    access_token?: string;
    refresh_token?: string;
}
