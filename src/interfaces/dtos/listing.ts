export interface ListingResponse {
    results: {
        hits: Listing[];
        nbPages: number;
    }[];
}

export interface Listing {
    id: number;
    uuid: string;
    category_id: number;
    added: number;
    created_at: number;
    description: {
        en: string;
        ar: string;
    };
    name: {
        en: string;
        ar: string;
    };
    absolute_url: {
        en: string;
        ar: string;
    };
    auto_agent_id: number;
    agent: {
        id: number;
        logo: string;
        name: {
            en: string;
            ar: string;
        };
    };
}
