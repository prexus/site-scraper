// Extracted from page source
export interface Lister {
    active_listings_count: number;
    agent_type: string;
    encrypted_user_id: string;
    id: string;
    initials: string;
    is_agent: boolean;
    is_email_verified: boolean;
    is_facebook_verified: boolean;
    is_phone_number_verified: boolean;
    is_sms_enabled: boolean;
    joined_timestamp: number;
    legacy_id: number;
    name: string;
    photo_url: string;
}
