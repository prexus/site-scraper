// Extracted from page source
export interface Context {
    taxonomy: {
        data: string;
    };
    listings: {
        currentListing: number;
        isLoading: boolean;
        loadingError: string;
        hasLoaded: boolean;
        [key: number]: ContextListing;
    };
}

interface ContextListing {
    details: {
        userId: number;
    };
}
