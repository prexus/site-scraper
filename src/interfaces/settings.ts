export interface Settings {
    paths: PathSettings;
    proxy: ProxySettings;
    logging: LoggingSettings;
    workers: WorkerSettings;
    metrics: ServerSettings;
    webServer: ServerSettings;
    credentials: CredentialsSettings;
}

export interface LoggingSettings {
    level: string;
    loki: string;
}

export interface ServerSettings {
    protocol?: string;
    host?: string;
    path?: string;
    port?: number;
    user?: string;
    pass?: string;
}

export interface PathSettings {
    dataPath: string;
    scrapesPath: string;
    exportPath: string;
    logPath: string;
}

export interface WorkerSettings {
    listingWorkerActive: boolean;
    listingWorkerRescan: boolean;
    listingWorkerMaxPage: number;
    scrapeWorkersActive: number;
}

export interface ProxySettings {
    source: string;
}

export interface CredentialsSettings {
    source: string;
}

export interface RecaptchaSettings {
    apiKey: string;
}
