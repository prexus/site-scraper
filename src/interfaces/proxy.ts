export interface Proxy {
    url: string;
    username: string;
    password: string;
    destination: string;
}
