import { Category } from '@enums/category';

export interface Scrape {
    id: number;
    created: number;
    started: number | null;
    completed: number | null;
    failed: number | null;
    retries: number | null;
    category: Category;
    identifier: string;
}
