export interface Credentials {
    email: string;
    password: string;
    banned: boolean;
    accessToken?: string;
    refreshToken?: string;
}
