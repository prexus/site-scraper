import { Category } from 'enums/category';

export interface SiteScraperAPI {
    type: Category;
    typeIds?: number[];
    index: string;
    facets?: string[];
    attributes: string[];

    filters: string;

    apiSource: string;
    apiEndpoint?: string;
    apiKey?: string;
    apiAppId?: string;
    apiExpiry?: Date;
}
