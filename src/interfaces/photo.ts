import { PhotoType } from '@enums/photo';

export interface Photo {
    url: string;
    type: PhotoType;
    file: string;
}
