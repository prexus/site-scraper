import winston from 'winston';

export default interface Logger extends winston.Logger {
    start: Function;
}
