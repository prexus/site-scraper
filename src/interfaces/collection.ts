import { Meta } from '@interfaces/meta';
import { Photo } from '@interfaces/photo';

import { Lister } from '@dtos/lister';
import { Listing } from '@dtos/listing';
import { Context } from '@dtos/context';
import { ProfileReponse } from '@dtos/profile';

export interface Collection {
    listing: Listing;
    screenshot: Buffer;
    source: string;
    extract: Extract;
    profile: ProfileReponse;
    meta?: Meta;
}

export interface Extract {
    description: string;
    context: Context;
    lister: Lister;
    photos: Photo[];
    primaryPhoneNumber: string;
    inlinePhoneNumbers: string[];
}
