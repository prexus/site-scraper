export class BotDetectionError extends Error {
    constructor(msg: string) {
        super(msg);

        this.name = this.constructor.name;

        Object.setPrototypeOf(this, BotDetectionError.prototype);
    }
}
