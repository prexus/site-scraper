import { Credentials } from '@interfaces/credentials';

import repositoryService from '@services/repository.service';
import settingsService from '@services/settings.service';
import utilityService from '@services/utility.service';

const service = {
    getCredentials,
    markAsBanned
};

let credentials: Credentials[] = null;

async function getAllCredentials() {
    if (!credentials) {
        const settings = settingsService.getCredentialsSettings();
        const source = settings.source;

        credentials = utilityService.jsonLoad<Credentials[]>(source);
    }

    credentials = await setBanStatus(credentials);

    return credentials;
}

async function setBanStatus(credentialsInput: Credentials[]) {
    const bans = await repositoryService.getBans();

    for (const entry of credentialsInput) {
        const banned = bans.some((ban) => ban.email === entry.email);

        entry.banned = banned;
    }

    return credentialsInput;
}

async function markAsBanned(bannedCredentials: Credentials) {
    const allCredentials = await getAllCredentials();
    const foundCredentials = allCredentials.find((c) => c.email === bannedCredentials.email);

    foundCredentials.banned = true;

    await repositoryService.addBan(bannedCredentials.email);
}

async function getCredentials() {
    const allCredentials = await getAllCredentials();
    const validCredentials = allCredentials.find((c) => !c.banned);

    return validCredentials;
}

export default service;
