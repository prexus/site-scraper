import express from 'express';

import metricsService from '@services/metrics.service';
import settingsService from '@services/settings.service';

const app = express();

const service = {
    start
};

function start(): void {
    const metricsSettings = settingsService.getMetricsSettings();
    const metricsPath = metricsSettings.path;
    app.get(metricsPath, metricsService.handleMetricsRequest);

    const serverSettings = settingsService.getWebServerSettings();
    const serverPort = serverSettings.port;
    app.listen(serverPort);
}

export default service;
