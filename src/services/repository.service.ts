import path from 'path';

import sqlite3 from 'sqlite3';
import { Database, open } from 'sqlite';

import { Scrape } from '@interfaces/scrape';
import { Metric } from '@interfaces/metric';
import { Ban } from '@interfaces/ban';

import { Category } from '@enums/category';

import settingsService from '@services/settings.service';

const service = {
    takeNextScrape,
    completeScrape,
    createScrape,
    failScrape,
    hasScraped,

    addMetric,
    takeMetrics,

    getPendingScrapes,

    addBan,
    getBans,
    isBanned
};

let db: Database = null;

async function initialize() {
    if (db) {
        return;
    }

    const settings = settingsService.getPathSettings();
    const folder = settings.dataPath;
    const file = 'database.sqlite';

    db = await open({
        filename: path.join(folder, file),
        driver: sqlite3.Database
    });

    const scrapeTable = `CREATE TABLE IF NOT EXISTS scrape (
        id           INTEGER     PRIMARY KEY,
        created      TIMESTAMP   DEFAULT (UNIXEPOCH()),
        started      TIMESTAMP   DEFAULT NULL,
        completed    TIMESTAMP   DEFAULT NULL,
        failed       TIMESTAMP   DEFAULT NULL,
        retries      INTEGER     DEFAULT NULL,
        category     TEXT        NOT NULL,
        identifier   TEXT        UNIQUE NOT NULL
    )`;

    const scrapeIndex = `
        CREATE INDEX IF NOT EXISTS
            scrape_identifier
        ON
            scrape (identifier);
    `;

    const metricTable = `CREATE TABLE IF NOT EXISTS metric (
        id           INTEGER     PRIMARY KEY,
        created      TIMESTAMP   DEFAULT (UNIXEPOCH()),
        type         TEXT        NOT NULL,
        method       TEXT        NOT NULL,
        data         TEXT        DEFAULT NULL
    )`;

    const banTable = `CREATE TABLE IF NOT EXISTS ban (
        id           INTEGER     PRIMARY KEY,
        created      TIMESTAMP   DEFAULT (UNIXEPOCH()),
        email        TEXT        UNIQUE NOT NULL
    )`;

    await db.exec(scrapeTable);
    await db.exec(scrapeIndex);

    await db.exec(metricTable);

    await db.exec(banTable);
}

async function createScrape(category: Category, identifier: string) {
    await initialize();

    const result = await db.run('INSERT INTO scrape (category, identifier) VALUES (?, ?)', category, identifier);

    return result.lastID;
}

async function failScrape(scrapeId: number): Promise<boolean> {
    await initialize();

    const result = await db.run(`
        UPDATE scrape
        SET    failed = UNIXEPOCH(),
               retries = IFNULL(retries, 0)
        WHERE  id = ?
    `, scrapeId);

    return (result.changes === 1);
}

async function hasScraped(category: Category, identifier: string) {
    await initialize();

    const result = await db.get('SELECT id FROM scrape WHERE category = ? AND identifier = ?', category, identifier);

    return !!result;
}

async function takeNextScrape(): Promise<Scrape> {
    await initialize();

    // Select next scrape based on the following queries
    const waiting = 'started IS NULL';
    const failed = '( DATETIME(failed, "unixepoch") < DATETIME("now", "-10 MINUTES") AND retries < 3 )';
    const stuck = '( DATETIME(started, "unixepoch") < DATETIME("now", "-30 MINUTES") AND completed IS NULL AND failed IS NULL )';

    // Takes the oldest registered scrape that has either:
    //
    //   1: Not been started yet
    //   2: Failed less than 3 times with
    //      last fail less than X time ago
    //
    // Cannot use LIMIT with UPDATE.
    // See https://github.com/TryGhost/node-sqlite3/issues/306
    const scrape = await db.get(`
        UPDATE scrape
        SET    started = UNIXEPOCH(),
               failed = NULL,
               retries = retries + 1 /* This evaluates to NULL if retries is already NULL */
        WHERE  id IN (
            SELECT id
            FROM   scrape
            WHERE  ${waiting} OR ${failed} OR ${stuck}
            ORDER  BY created ASC
            LIMIT  1
        )
        RETURNING *
    `);

    return scrape;
}

async function completeScrape(scrapeId: number): Promise<Scrape> {
    await initialize();

    const scrape = await db.get(`
        UPDATE    scrape
        SET       completed = UNIXEPOCH()
        WHERE     id = ?
        RETURNING *
    `, scrapeId);

    return scrape;
}

async function addMetric(type: string, method: string, data?: any) {
    await initialize();

    const dataValue = data ? JSON.stringify(data) : null;
    const result = await db.run('INSERT INTO metric (type, method, data) VALUES (?, ?, ?)', type, method, dataValue);

    return result.lastID;
}

async function takeMetrics() {
    await initialize();

    const metrics = await db.all<Metric[]>('DELETE FROM metric RETURNING *');

    return metrics;
}

async function getPendingScrapes() {
    await initialize();

    const scrapes = await db.all(`
        SELECT   category,
                 IFNULL(COUNT(id), 0) AS pending
        FROM     scrape
        WHERE    started IS NULL
        GROUP BY category
    `);

    return scrapes;
}

async function getBans() {
    await initialize();

    const bans = await db.all<Ban[]>('SELECT * FROM ban');

    return bans;
}

async function isBanned(email: string) {
    await initialize();

    const ban = await db.get('SELECT id FROM ban WHERE email = ?', email);

    return !!ban;
}

async function addBan(email: string) {
    await initialize();

    const result = await db.run('INSERT OR IGNORE INTO ban (email) VALUES (?)', email);

    return result.lastID;
}

export default service;
