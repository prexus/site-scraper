import fs from 'fs';

import { parsePhoneNumber as parseSinglePhoneNumber } from 'libphonenumber-js';
import { JSDOM } from 'jsdom';

const service = {
    json,
    jsonEval,
    jsonLoad,
    sleep,
    checksum,
    cleanHtml,
    timeRemaining,
    parsePhoneNumber,
    parsePhoneNumbers,
    toByteSize
};

function checksum(s: string) {
    let chk = 0x12345678;
    const len = s.length;
    for (let i = 0; i < len; i++) {
        chk += (s.charCodeAt(i) * (i + 1));
    }

    // eslint-disable-next-line no-bitwise
    return (chk & 0xffffffff).toString(16);
}

function sleep(minSeconds: number, maxSeconds?: number) {
    let seconds = minSeconds;

    if (maxSeconds) {
        seconds = Math.floor(Math.random() * (maxSeconds - minSeconds + 1)) + minSeconds;
    }

    return new Promise<void>((resolve) => {
        setTimeout(() => resolve(), seconds * 1000);
    });
}

function cleanHtml(html: string) {
    if (!html) {
        return null;
    }

    const rawDescription = html.replaceAll('<br>', '\n');
    const jsdom = new JSDOM(rawDescription);
    const clean = jsdom.window.document.body.textContent;

    return clean;
}

function timeRemaining(secondsRemaining: number) {
    const diffDays = Math.floor(secondsRemaining / 86400); // days
    const diffHrs = Math.floor((secondsRemaining % 86400) / 3600); // hours
    const diffMins = Math.floor(((secondsRemaining % 86400) % 3600) / 60); // minutes
    const diffSecs = Math.floor((((secondsRemaining % 86400) % 3600) % 60)); // seconds

    const diffArr = [];

    if (diffDays > 0) {
        diffArr.push(`${diffDays} days`);
    }

    if (diffHrs > 0) {
        diffArr.push(`${diffHrs} hours`);
    }

    if (diffMins > 0) {
        diffArr.push(`${diffMins} minutes`);
    }

    if (diffSecs > 0) {
        diffArr.push(`${diffSecs} seconds`);
    }

    return diffArr.join(', ').replace(/, ([^,]*)$/, ' and $1');
}

function jsonLoad<T>(file: string): T | null {
    const stringData = fs.readFileSync(file, 'utf-8');
    const parsedData = json<T>(stringData);

    return parsedData;
}

function json<T>(text: string): T | null {
    try {
        return JSON.parse(text);
    } catch (e) {
        return null;
    }
}

// This function is made as JSON.parse has issues with
// parsing strings containing arabic characters. It's a
// bit strange and could be me doing something wrong but
// something weird is happening. Uncomment the following
// line to see quotation marks switching sides:
//
//   { "ar": "دراجة نارية \\"شوبر"\\" }
//
// I have been unable to get it to work. But this does!
function jsonEval<T>(text: string): T | null {
    try {
        return global.eval(`JSON.parse("${text}")`);
    } catch (e) {
        return null;
    }
}

function parsePhoneNumbers(inputPhoneNumbers: string[], filterPrimaryNumber?: string): string[] {
    let parsedPhoneNumbers = inputPhoneNumbers.map((no) => parsePhoneNumber(no));
    parsedPhoneNumbers = parsedPhoneNumbers.filter(Boolean);

    if (filterPrimaryNumber) {
        parsedPhoneNumbers = parsedPhoneNumbers.filter((no) => no !== filterPrimaryNumber);
    }

    const uniquePhoneNumbers = new Set(parsedPhoneNumbers);

    return [...uniquePhoneNumbers];
}

function parsePhoneNumber(phoneNumber: string): string {
    try {
        const parsedPhoneNumber = parseSinglePhoneNumber(phoneNumber, 'AE');
        const phoneNumberAsE164 = parsedPhoneNumber?.number;

        return phoneNumberAsE164 ?? phoneNumber;
    } catch (e) {
        return null;
    }
}

function toByteSize(bytes: number) {
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let unitIndex = 0;
    let size = bytes;

    while (size >= 1024 && ++unitIndex) {
        size /= 1024;
    }

    const decimals = size < 10 && unitIndex > 0 ? 1 : 0;
    const roundedSize = size.toFixed(decimals);
    const unit = units[unitIndex];

    return `${roundedSize} ${unit}`;
}

export default service;
