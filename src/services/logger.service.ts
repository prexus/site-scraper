import path from 'path';

import winston from 'winston';
import LokiTransport from 'winston-loki';

import Logger from '@interfaces/logger';

import settingsService from '@services/settings.service';
import { Color } from '@enums/colors';

function start(label = 'unlabeled') {
    const settings = settingsService.getSettings();
    const logFile = path.join(settings.paths.logPath, 'output.ansi');

    const colors = {
        error: Color.RED,
        warn: Color.YELLOW,
        info: Color.MAGENTA,
        verbose: Color.GREEN,
        debug: Color.GREEN
    };

    const lineFormat = winston.format.printf((info) => {
        const timestamp = info.timestamp;
        const level = info.level.toLocaleUpperCase().padEnd(7);
        const error = info.error || '';
        const message = info.message;

        return `${colors[info.level]} ${timestamp} ${level}: ${message} ${error}${Color.RESET}`;
    });

    const textFormat = winston.format.combine(
        winston.format.timestamp(),
        lineFormat
    );

    logger.level = settings.logging.level;
    logger.format = textFormat;

    logger.add(new winston.transports.Console());
    logger.add(new winston.transports.File({ filename: logFile }));

    if (settings.logging.loki) {
        const lokiHost = settings.logging.loki;
        const lokiFormat = winston.format.combine(
            winston.format.label({ label }),
            winston.format.json()
        );
        const lokiOptions = {
            host: lokiHost,
            level: 'debug',
            format: lokiFormat
        };

        logger.add(new LokiTransport(lokiOptions));
    }
}

const logger = winston.createLogger() as Logger;
logger.start = start;

export default logger;
