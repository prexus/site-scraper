import fs from 'fs';
import path from 'path';

import archiver from 'archiver';

import loggerService from '@services/logger.service';

const service = {
    compress
};

async function compress(source: string, destination: string) {
    const output = await getWriteStream(destination);
    const archiveCompleted = getCompleteHandler(output);

    const options = getCompressorOptions();
    const archive = archiver('zip', options);

    registerErrorHandlers(output, archive);

    archive.pipe(output);
    archive.directory(source, false);

    await archive.finalize();
    await archiveCompleted;
}

async function getWriteStream(destination: string) {
    await ensureDestinationPath(destination);

    return fs.createWriteStream(destination);
}

async function ensureDestinationPath(destination: string) {
    const fullPath = path.parse(destination);
    const options = { recursive: true };

    await fs.promises.mkdir(fullPath.dir, options);
}

function getCompressorOptions() {
    const options = {
        zlib: { level: 1 }
    };

    return options;
}

function getCompleteHandler(output: fs.WriteStream) {
    return new Promise<void>((resolve, reject) => {
        output
            .on('close', () => { resolve(); })
            .on('error', (e) => { reject(e); });
    });
}

function registerErrorHandlers(output: fs.WriteStream, archive: archiver.Archiver) {
    output.on('error', (e) => {
        output.end();

        throw new Error(`[ARCHIVE SERVICE] Output failed: ${e}`);
    });

    archive.on('warning', (e) => {
        output.end();

        loggerService.warn(`[ARCHIVE SERVICE] Archive produced a warning: ${e}`);
    });

    archive.on('error', (e) => {
        output.end();

        throw new Error(`[ARCHIVE SERVICE] Archive failed: ${e}`);
    });
}

export default service;
