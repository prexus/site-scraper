import { Request, Response } from 'express';

import { collectDefaultMetrics, register } from 'prom-client';

import scrapeMetrics from '@metrics/scrape.metrics';
import captchaMetrics from '@metrics/captcha.metrics';
import listingMetrics from '@metrics/listing.metrics';

import repositoryService from '@services/repository.service';

const service = {
    start,
    handleMetricsRequest
};

const metricsCollection = {
    ...scrapeMetrics,
    ...captchaMetrics,
    ...listingMetrics
};

async function start() {
    collectDefaultMetrics();

    await collectWorkerMetrics();
}

async function handleMetricsRequest(_request: Request, response: Response) {
    const metrics = await register.metrics();

    response.send(metrics);
}

async function collectWorkerMetrics() {
    const metrics = await repositoryService.takeMetrics();

    for (const metric of metrics) {
        const type = metric.type;
        const method = metric.method;
        const data = metric.data;

        const metricHandler = metricsCollection[type];
        const metricFunction = metricHandler[method].bind(metricHandler);
        const metricArgs = data ? JSON.parse(data) : null;

        if (Array.isArray(metricArgs)) {
            metricFunction(...metricArgs);
        } else {
            metricFunction(metricArgs);
        }
    }

    setTimeout(collectWorkerMetrics, 1000);
}

export default service;
