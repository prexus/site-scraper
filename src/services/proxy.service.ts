import { Proxy } from '@interfaces/proxy';

import settingsService from '@services/settings.service';
import utilityService from '@services/utility.service';

const service = {
    getProxy,
    getProxies
};

let proxies = null;

function getProxies() {
    if (!proxies) {
        const settings = settingsService.getProxySettings();
        const source = settings.source;

        proxies = utilityService.jsonLoad<Proxy[]>(source);
    }

    return proxies;
}

function getProxy(index: number) {
    const p = getProxies();
    const i = index % p.length;

    return p[i];
}

export default service;
