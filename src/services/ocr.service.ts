import { createWorker } from 'tesseract.js';

const service = {
    extractPhoneNo
};

let worker = null;

async function extractPhoneNo(base64Image: string) {
    if (!worker) {
        worker = createWorker();

        await worker.load();
        await worker.loadLanguage('eng');
        await worker.initialize('eng');
    }

    const result = await worker.recognize(base64Image);

    // We are not terminating the worker anymore now that
    // we only create/load/init once. Hopefully this will
    // elliminate the errors we see on long running sessions
    //
    // await worker.terminate();

    return result.data.text;
}

export default service;
