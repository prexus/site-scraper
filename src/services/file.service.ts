import fs from 'fs';
import path from 'path';

import FileType from 'file-type';

import loggerService from '@services/logger.service';
import archiverService from '@services/archiver.service';
import settingsService from '@services/settings.service';

import { FilesMeta } from '@interfaces/meta';
import { Scrape } from '@interfaces/scrape';

const service = {
    getScrapePath,
    download,
    compress,
    clean,
    load,
    save
};

function getScrapePath(scrapeId: number) {
    const settings = settingsService.getPathSettings();
    const scrapePath = path.join(settings.scrapesPath, scrapeId.toString());

    return scrapePath;
}

function getExportPath(scrape: Scrape) {
    const settings = settingsService.getPathSettings();
    const exportPath = path.join(settings.exportPath, `${scrape.identifier}.scrape`);

    return exportPath;
}

async function load(scrapeId: number, fileName: string) {
    const scrapePath = getScrapePath(scrapeId);
    const targetPath = path.join(scrapePath, fileName);

    const data = await fs.promises.readFile(targetPath, 'utf8');

    return JSON.parse(data);
}

async function save(scrapeId: number, fileName: string, data: string | object | Buffer) {
    if (!data) {
        return;
    }

    const scrapePath = getScrapePath(scrapeId);
    const targetPath = path.join(scrapePath, fileName);

    const content = getWritableData(data);

    await fs.promises.mkdir(scrapePath, { recursive: true });
    await fs.promises.writeFile(targetPath, content);

    const parsedPath = path.parse(targetPath);
    const parsedFile = parsedPath.name;

    const files: Partial<FilesMeta> = {};
    files[parsedFile] = fileName;
}

function getWritableData(data: string | object | Buffer) {
    if (typeof data === 'string' || Buffer.isBuffer(data)) {
        return data;
    }

    return JSON.stringify(data, null, 4);
}

async function compress(scrape: Scrape) {
    const scrapePath = getScrapePath(scrape.id);
    const exportPath = getExportPath(scrape);

    await archiverService.compress(scrapePath, exportPath);

    return exportPath;
}

async function clean(scrapeId: number) {
    const scrapePath = getScrapePath(scrapeId);
    const options = { recursive: true, force: true };

    try {
        await fs.promises.rm(scrapePath, options);
    } catch (error) {
        loggerService.warn(`[FILE SERVICE] Unable to remove ${scrapePath}: ${error.message}`);
    }
}

async function download(scrapeId: number, url: string, file: string) {
    let response: Response = null;

    try {
        response = await fetch(url);
    } catch (error) {
        loggerService.warn(`[FILE SERVICE] Download of ${url} failed with: ${error.message}`);

        return null;
    }

    if (response.status !== 200) {
        loggerService.warn(`[FILE SERVICE] Server responded with status ${response.status} when downloading ${url}`);

        return null;
    }

    const scrapePath = getScrapePath(scrapeId);
    const tmpFile = path.join(scrapePath, `${file}.tmp`);

    const fileTargetStream = fs.createWriteStream(tmpFile);
    const writeableStream = new WritableStream({
        write(chunk) { fileTargetStream.write(chunk); },
        close() { fileTargetStream.close(); }
    });

    // While pipeTo is could be awaited there is a chance our targetStream
    // is not yet done writing. This is the case with very small files where
    // the content could still be in the buffer and not written to disk yet.
    // Therefore we will wait until we get the finish signal from that too.
    const writeStreamDone = response.body.pipeTo(writeableStream);
    const targetStreamDone = new Promise((resolve) => { fileTargetStream.on('finish', resolve); });

    await Promise.all([writeStreamDone, targetStreamDone]);

    // Create target file type with correct extension
    const fileType = await FileType.fromFile(tmpFile);
    const fileName = `${file}.${fileType?.ext ?? '.unknown'}`;
    const targetFile = path.join(scrapePath, fileName);

    // We used to rename, but that would sometimes fail due to
    // things out of our control (OS-related). Now we copy it over
    // and delete the tmp file instead. This should ensure that
    // we at least get the filename correct and maybe once in
    // a while we are left with an extra tmp file if the unlink
    // fails - but what can you do...
    try {
        await fs.promises.copyFile(tmpFile, targetFile);
        await fs.promises.unlink(tmpFile);
    } catch (error) {
        loggerService.warn(`[FILE SERVICE] Failed copying or unlinking file with ${error.message}`);
    }

    if (!fileType) {
        loggerService.warn(`[FILE SERVICE] Unable to determine file type for ${url}`);
    }

    return fileName;
}

export default service;
