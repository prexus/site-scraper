import loggerService from '@services/logger.service';

const service = {
    start
};

async function start() {
    loggerService.info('');
    loggerService.info('  _________.__  __           _________                                                       ');
    loggerService.info(' /   _____/|__|/  |_  ____  /   _____/ ____ _____ _____________   ___________                ');
    loggerService.info(' \\_____  \\ |  \\   __\\/ __ \\ \\_____  \\_/ ___\\\\__  \\\\_  __ \\____ \\_/ __ \\_  __ \\');
    loggerService.info(' /        \\|  ||  | \\  ___/ /        \\  \\___ / __ \\|  | \\/  |_> >  ___/|  | \\/        ');
    loggerService.info('/_______  /|__||__|  \\___  >_______  /\\___  >____  /__|  |   __/ \\___  >__|               ');
    loggerService.info('        \\/               \\/        \\/     \\/     \\/      |__|        \\/                ');
    loggerService.info('');
}

export default service;
