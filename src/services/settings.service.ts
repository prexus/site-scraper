import fs from 'fs';
import path from 'path';

import { PuppeteerLaunchOptions } from 'puppeteer';

import { PathSettings, LoggingSettings, Settings, WorkerSettings, ServerSettings, ProxySettings, CredentialsSettings, RecaptchaSettings } from '@interfaces/settings';
import { Proxy } from '@interfaces/proxy';

import loggerService from './logger.service';

const service = {
    start,
    getSettings,
    getPathSettings,
    getProxySettings,
    getWorkerSettings,
    getMetricsSettings,
    getLoggingSettings,
    getRecapchaSettings,
    getWebServerSettings,
    getPuppeteerSettings,
    getCredentialsSettings
};

async function start() {
    const settings = getSettings();

    const workers = settings.workers;
    const metrics = settings.metrics;
    const webServer = settings.webServer;

    const metricsEndpointPath = `http://<host>:${webServer.port}${metrics.path}`;

    const scrapeWorkerStatus = `x${settings.workers.scrapeWorkersActive}`;
    let listingWorkerStatus = `x${workers.listingWorkerActive ? 1 : 0}`;

    if (workers.listingWorkerRescan) {
        listingWorkerStatus += ', rescan';
    }

    if (workers.listingWorkerMaxPage) {
        listingWorkerStatus += `, to pg. ${workers.listingWorkerMaxPage}`;
    }

    loggerService.info('');
    loggerService.info('Settings:');
    loggerService.info(`  - Data path          ${settings.paths.dataPath}`);
    loggerService.info(`  - Scrapes path       ${settings.paths.scrapesPath}`);
    loggerService.info(`  - Exports path       ${settings.paths.exportPath}`);
    loggerService.info(`  - Metrics            ${metricsEndpointPath}`);
    loggerService.info('');
    loggerService.info('Data sources:');
    loggerService.info(`  - Proxies            ${settings.proxy.source}`);
    loggerService.info(`  - Credentials        ${settings.credentials.source}`);
    loggerService.info('');
    loggerService.info('Workers:');
    loggerService.info(`  - Listing worker     ${listingWorkerStatus}`);
    loggerService.info(`  - Scrape workers     ${scrapeWorkerStatus}`);
    loggerService.info('');
}

function getSettings(): Settings {
    return {
        paths: getPathSettings(),
        proxy: getProxySettings(),
        logging: getLoggingSettings(),
        workers: getWorkerSettings(),
        metrics: getMetricsSettings(),
        webServer: getWebServerSettings(),
        credentials: getCredentialsSettings()
    };
}

function getPathSettings(): PathSettings {
    const data = process.env.DATA_PATH || 'data';
    const scrapes = process.env.SCRAPES_PATH || 'scrapes';
    const exports = process.env.EXPORTS_PATH || 'exports';
    const logs = process.env.LOGS_PATH || 'logs';

    const paths = {
        dataPath: data,
        scrapesPath: path.join(data, scrapes),
        exportPath: path.join(data, exports),
        logPath: path.join(data, logs)
    };

    for (const property of Object.keys(paths)) {
        fs.mkdirSync(paths[property], { recursive: true });
    }

    return paths;
}

function getProxySettings(): ProxySettings {
    const settings = getPathSettings();
    const filename = process.env.PROXIES_FILE || 'proxies.json';
    const source = path.join(settings.dataPath, filename);

    return { source };
}

function getCredentialsSettings(): CredentialsSettings {
    const settings = getPathSettings();
    const filename = process.env.CREDENTIALS_FILE || 'credentials.json';
    const source = path.join(settings.dataPath, filename);

    return { source };
}

function getLoggingSettings(): LoggingSettings {
    return {
        level: process.env.LOG_LEVEL || 'debug',
        loki: process.env.LOG_LOKI || null
    };
}

function getWebServerSettings(): ServerSettings {
    const port = process.env.WEB_SERVER_PORT;

    return {
        port: port ? Number(port) : 8080
    };
}

function getMetricsSettings(): ServerSettings {
    return {
        path: process.env.METRICS_PATH || '/metrics'
    };
}

function getPuppeteerSettings(proxy?: Proxy): PuppeteerLaunchOptions {
    const settings = {
        headless: false,
        args: [
            '--window-size=1280,1024'
        ],
        slowMo: 10,
        executablePath: getChromePath()
    };

    if (proxy) {
        settings.args.push(`--proxy-server=${proxy.url}`);
    }

    return settings;
}

function getChromePath() {
    const chromePaths = [
        'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe',
        'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe'
    ];

    for (const chromePath of chromePaths) {
        if (fs.existsSync(chromePath)) {
            return chromePath;
        }
    }

    // Defaults to Chromium
    return null;
}

function getRecapchaSettings(): RecaptchaSettings {
    return {
        apiKey: process.env.API_KEY_2CAPTCHA
    };
}

function getWorkerSettings(): WorkerSettings {
    const scrapeWorkers = process.env.SCRAPE_WORKERS ? parseInt(process.env.SCRAPE_WORKERS, 10) : 0;
    const maxPage = getListingWorkerMaxPage();

    const settings = {
        listingWorkerActive: (process.env.LISTING_WORKER === '1'),
        listingWorkerRescan: (process.env.LISTING_WORKER_RESCAN === '1'),
        listingWorkerMaxPage: maxPage,
        scrapeWorkersActive: scrapeWorkers
    };

    return settings;
}

function getListingWorkerMaxPage() {
    const maxPage = process.env.LISTING_WORKER_MAX_PAGE;

    return maxPage ? Number(maxPage) : null;
}

export default service;
