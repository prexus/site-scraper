import { spawn, Worker } from 'threads';

import proxyService from '@services/proxy.service';
import loggerService from '@services/logger.service';
import utilityService from '@services/utility.service';
import settingsService from '@services/settings.service';

const settings = settingsService.getWorkerSettings();

let proxyIndex = 0;

const service = {
    start
};

async function start() {
    if (settings.listingWorkerActive) {
        await startListingWorker();
    }

    if (settings.scrapeWorkersActive > 0) {
        await startScrapeWorkers();
    }

    loggerService.verbose('[WORKER SERVICE] Workers initialized!');
}

async function startListingWorker() {
    loggerService.info('[WORKER SERVICE] Listing worker initialized');

    const listingWorker = new Worker('../workers/listing.worker');
    const listingWorkerTask = await spawn(listingWorker);

    listingWorkerTask().catch(async (error) => {
        loggerService.error('[WORKER SERVICE] Listing worker failed but will restart soon', error);

        await utilityService.sleep(30);

        loggerService.error('[WORKER SERVICE] Listing worker restarting');

        await startListingWorker();
    });
}

async function startScrapeWorkers() {
    for (let workerNo = 0; workerNo < settings.scrapeWorkersActive; workerNo++) {
        const workerName = `WORKER-#${workerNo + 1}`;

        await queueScrapeWorker(workerName);

        // A little relief to not peak the
        // CPU too much when starting all
        // the browser windows.
        await utilityService.sleep(5);
    }
}

async function queueScrapeWorker(workerName: string) {
    const scrapeWorker = new Worker('../workers/scrape.worker');
    const scrapeWorkerTask = await spawn(scrapeWorker);

    // Gets the next proxy, supports overflow
    const proxy = proxyService.getProxy(proxyIndex++);

    scrapeWorkerTask(workerName, proxy).catch((error: Error) => {
        loggerService.error(`[WORKER SERVICE] Scrape worker [${workerName}] failed with '${error.message}' but will restart soon`);
    }).finally(async () => {
        await utilityService.sleep(30);

        loggerService.error(`[WORKER SERVICE] Scrape worker [${workerName}] restarting`);

        await queueScrapeWorker(workerName);
    });

    loggerService.info(`[WORKER SERVICE] Scrape worker [${workerName}] initialized`);
}

export default service;
