import { ElementHandle, Page } from 'puppeteer';
import { Solver } from '2captcha';

import loggerService from '@services/logger.service';
import utilityService from '@services/utility.service';
import settingsService from '@services/settings.service';

import metricsHelper from '@helpers/metrics.helper';

const service = {
    checkReCAPTCHA,
    botDetected,
    waitForNetworkIdle,
    waitForHtmlRender,
    checkForAgentButton,
    expandDescription,
    expandAmenities,
    checkForPhoneNumberButtons,
    expandMap,
    clean
};

async function checkReCAPTCHA(page: Page) {
    const pageContent = await page.content();
    const isBotDetected = botDetected(pageContent);

    // All good
    if (!isBotDetected) {
        return true;
    }

    loggerService.info('Detected reCAPTCHA - attempting to solve...');

    await metricsHelper.detectedCaptchas();

    // Find the frame content
    const incapsulaFrame = page.mainFrame().childFrames()[0];
    const frameContent = await incapsulaFrame.content();

    // Find site key
    const sitekeyPattern = 'data-sitekey="(?<key>.*?)"';
    const sitekeyRegex = new RegExp(sitekeyPattern);
    const sitekeyMatch = sitekeyRegex.exec(frameContent);
    const sitekey = sitekeyMatch.groups.key;

    // Solve
    const settings = settingsService.getRecapchaSettings();
    const solver = new Solver(settings.apiKey);

    try {
        const solved = await solver.recaptcha(sitekey, page.url());
        const trigger = (data: string) => { this.onCaptchaFinished(data); };

        loggerService.info(`Solved reCAPTCHA - Data is ${solved.data.substring(0, 20)}...`);

        await metricsHelper.solvedCaptchas();

        await incapsulaFrame.evaluate(trigger, solved.data);

        await utilityService.sleep(5);

        return true;
    } catch (e) {
        loggerService.error(`Unable to solve reCAPTCHA. Failed with: ${e.message}`);

        await metricsHelper.failedCaptchas();
    }

    return false;
}

function botDetected(html: string) {
    const searchString = 'Request unsuccessful. Incapsula incident ID:';
    const isBotDetected = html.includes(searchString);

    return isBotDetected;
}

async function waitForNetworkIdle(page: Page, timeout = 5000) {
    // Use this if you dont want to throw an exception
    // in case the network isn't idling after the
    // specified timeout.
    try {
        await page.waitForNetworkIdle({ timeout });
    } catch (e) {
        // Continue
    }
}

async function waitForHtmlRender(page: Page, timeout = 10000) {
    // If 3 stable iterations required it needs 4
    // checks to be marked as stable as the first
    // is a change and resets the counter to 0
    const stableSizeIterationsRequired = 3;
    const checkDurationDelaySeconds = 1;

    const maxChecks = timeout / checkDurationDelaySeconds;
    let lastHTMLSize = 0;
    let checkCounts = 1;
    let countStableSizeIterations = 0;
    const checks = [];

    while (checkCounts++ <= maxChecks) {
        const html = await page.content();

        checks.push(html.length);

        if (lastHTMLSize !== 0 && html.length === lastHTMLSize) {
            countStableSizeIterations++;
        } else {
            countStableSizeIterations = 0;
        }

        if (countStableSizeIterations === stableSizeIterationsRequired) {
            return;
        }

        lastHTMLSize = html.length;

        if (checkCounts < maxChecks) {
            // Doing another iteration, but sleep first
            await utilityService.sleep(checkDurationDelaySeconds);
        }
    }
}

async function expandDescription(page: Page) {
    const targets = [
        '//button[contains(., "Show full description")]', // Motors, classified and community
        '//button[contains(., "See full description")]' // Properties
    ];

    return xPathClick(page, targets);
}

async function expandMap(page: Page) {
    const targets = [
        '//div[contains(text(), "Show Map")]', // Motors, classified and community
        '//button[@data-ui="show-map"]' // Properties
    ];

    return xPathClick(page, targets);
}

async function expandAmenities(page: Page) {
    const targets = [
        '//button[contains(., "Show more amenities")]' // Properties
    ];

    return xPathClick(page, targets);
}

async function checkForAgentButton(page: Page) {
    return click(page, '[data-fnid="agent-number-btn"]');
}

async function checkForPhoneNumberButtons(page: Page) {
    // Usually with agents on properties
    await click(page, '[class*=ShowPhoneNumberButton]');
    await click(page, 'button[data-target-id="show-phone-number"]');

    // Move cursor away to avoid focus
    await click(page, 'body');
}

async function click(page: Page, selector: string) {
    if (await page.$(selector)) {
        await page.click(selector);
    }
}

async function xPathClick(page: Page, xPathSelector: string[]) {
    const elementHandles = await page.$x(xPathSelector.join(' | '));
    if (elementHandles.length === 1) {
        const elementHandle = elementHandles[0] as ElementHandle<Element>;

        await elementHandle.evaluate((domElement) => {
            domElement.scrollIntoView();
        });

        await elementHandle.click();

        return true;
    }

    return false;
}

async function clean(page: Page) {
    // Remove modals
    await remove(page, 'div[site-scraper-portal-container]');
    await remove(page, 'div#login-modal-wrapper');

    // Remove popup
    await remove(page, '#moe-push-div');

    // Tooltips
    await remove(page, '[class*="StyledTooltip"]', true);

    // Remove floating headers
    await remove(page, '[data-testid="details-desktop"] > div:first-child');
    await remove(page, '[class*="DPVTopBar__Root"]');
}

async function remove(page: Page, selector: string, removeAll = false) {
    return page.evaluate((sel, all) => {
        if (all) {
            const list = document.querySelectorAll(sel);
            list.forEach((el) => el.remove());
        } else {
            const el = document.querySelector(sel);
            el?.remove();
        }
    }, selector, removeAll);
}

export default service;
