import './alias';

import loggerService from '@services/logger.service';
import workerService from '@services/worker.service';
import welcomeService from '@services/welcome.service';
import metricsService from '@services/metrics.service';
import settingsService from '@services/settings.service';
import webServerService from '@services/web-server.service';

async function start() {
    await loggerService.start();
    await welcomeService.start();
    await settingsService.start();
    await workerService.start();
    await metricsService.start();

    webServerService.start();
}

start().catch((error) => {
    loggerService.error(`[APP ERROR] ${error.message}`);
});
