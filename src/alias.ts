import moduleAlias from 'module-alias';

const aliases = [
    'classes',
    'errors',
    'definitions',
    'enums',
    'interfaces',
    'interfaces/dtos',
    'services',
    'metrics',
    'workers',
    'workers/helpers'
];

for (const alias of aliases) {
    moduleAlias.addAlias(`@${alias.split('/').at(-1)}`, `${__dirname}/${alias}`);
}
