import { Counter, Gauge } from 'prom-client';

import repositoryService from '@services/repository.service';

// - - -

const pendingScrapes = new Gauge({
    name: 'scraper_pending_scrapes',
    help: 'How many scrapes are waiting to start',
    labelNames: ['category'],
    async collect() {
        const scrapes = await repositoryService.getPendingScrapes();

        for (const scrape of scrapes) {
            const category = scrape.category;
            const pending = scrape.pending;

            this.set({ category }, pending);
        }
    }
});

const completedScrapes = new Counter({
    name: 'scraper_completed_scrapes',
    help: 'How many scrapes has completed',
    labelNames: ['worker', 'category']
});

const failedScrapes = new Counter({
    name: 'scraper_failed_scrapes',
    help: 'How many scrapes has failed',
    labelNames: ['worker', 'category']
});

// - - -

const metrics = {
    pendingScrapes,
    completedScrapes,
    failedScrapes
};

export default metrics;
