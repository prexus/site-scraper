import { Counter } from 'prom-client';

// - - -

const detectedCaptchas = new Counter({
    name: 'scraper_detected_captchas',
    help: 'How many captcha needs to be solved'
});

const solvedCaptchas = new Counter({
    name: 'scraper_solved_captchas',
    help: 'How many captcha was successfully solved'
});

const failedCaptchas = new Counter({
    name: 'scraper_failed_captchas',
    help: 'How many captcha failed solving'
});

// - - -

const metrics = {
    detectedCaptchas,
    solvedCaptchas,
    failedCaptchas
};

export default metrics;
