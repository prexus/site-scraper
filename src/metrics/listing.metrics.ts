import { Counter } from 'prom-client';

// - - -

const addedListings = new Counter({
    name: 'scraper_added_listings',
    help: 'How many listings has been added',
    labelNames: ['category']
});

// - - -

const metrics = {
    addedListings
};

export default metrics;
