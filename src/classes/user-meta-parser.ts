import utilityService from '@services/utility.service';
import loggerService from '@services/logger.service';

import { PhotoType } from '@enums/photo';

import { Collection, Extract } from '@interfaces/collection';
import { Photo } from '@interfaces/photo';

import { ProfileReponse } from '@dtos/profile';
import { Context } from '@dtos/context';
import { Listing } from '@dtos/listing';
import { Lister } from '@dtos/lister';

export class UserMetaParser {
    listing: Listing;
    profile: Partial<ProfileReponse>;
    extract: Extract;

    context: Partial<Context>;
    lister: Partial<Lister>;
    photos: Photo[];

    constructor(collection: Collection) {
        this.listing = collection.listing;
        this.profile = collection.profile ?? {};
        this.extract = collection.extract;

        this.context = this.extract.context ?? {};
        this.lister = this.extract.lister ?? {};
        this.photos = this.extract.photos;
    }

    public getCreated() {
        return this.lister.joined_timestamp ?? null;
    }

    public getId() {
        const listerId = this.lister.id;
        const contextUserId = this.getContextUserId();

        return listerId ?? contextUserId ?? null;
    }

    public getName() {
        const listingName = this.lister?.name;
        const profileName = this.profile?.full_name;

        return listingName ?? profileName ?? null;
    }

    public getCompany() {
        const english = this.listing.agent?.name.en;
        const arabic = this.listing.agent?.name.ar;

        return english ?? arabic ?? null;
    }

    public getCompanyId() {
        const propertyAgentId = this.listing.agent?.id.toString();
        const autoAgentId = this.listing.auto_agent_id?.toString();

        return propertyAgentId ?? autoAgentId ?? null;
    }

    public getProfilePhoto() {
        const filter = (photo: Photo) => photo.type === PhotoType.PROFILE_PHOTO;
        const photo = this.photos.find(filter);

        return photo ? photo.file : null;
    }

    public getPrimaryPhoneNumber() {
        return this.getPhoneNumbers().primaryPhoneNumber;
    }

    public getOtherPhoneNumber() {
        return this.getPhoneNumbers().otherPhoneNumbers;
    }

    private getPhoneNumbers() {
        let primaryPhoneNumber = this.profile.phone_number;
        const otherPhoneNumbers = this.extract.inlinePhoneNumbers || [];

        if (primaryPhoneNumber) {
            if (this.extract.primaryPhoneNumber) {
                otherPhoneNumbers.push(this.extract.primaryPhoneNumber);
            }
        } else {
            primaryPhoneNumber = this.extract.primaryPhoneNumber || null;
        }

        const primary = utilityService.parsePhoneNumber(primaryPhoneNumber);
        const others = utilityService.parsePhoneNumbers(otherPhoneNumbers, primary);

        return {
            primaryPhoneNumber: primary,
            otherPhoneNumbers: others
        };
    }

    private getContextUserId() {
        const listings = this.context.listings;
        let userId = null;

        if (listings) {
            const listingId = listings.currentListing;
            const listing = listings[listingId];
            const details = listing?.details;

            if (!details) {
                loggerService.error(`[USER META PARSER] Unable to extract listing details from context on listing ID ${listingId}`, listings);
            }

            userId = details?.userId?.toString() ?? null;
        }

        return userId;
    }
}
