import utilityService from '@services/utility.service';

import { PhotoType } from '@enums/photo';

import { Collection, Extract } from '@interfaces/collection';
import { Photo } from '@interfaces/photo';

import { Listing } from '@dtos/listing';

export class PageMetaParser {
    listing: Listing;
    extract: Extract;

    photos: Photo[];

    constructor(collection: Collection) {
        this.listing = collection.listing;
        this.extract = collection.extract;

        this.photos = this.extract.photos;
    }

    public getCreated() {
        return this.listing.created_at;
    }

    public getAdded() {
        return this.listing.added;
    }

    public getUrl() {
        return this.listing.absolute_url.en;
    }

    public getHeadline() {
        const english = this.listing.name.en;
        const arabic = this.listing.name.ar;

        return english ?? arabic ?? null;
    }

    public getContent() {
        const english = this.listing.description?.en;
        const arabic = this.listing.description?.ar;
        const html = this.extract.description;

        const selected = english ?? html ?? arabic ?? null;

        return utilityService.cleanHtml(selected);
    }

    public getPhotos() {
        const filterFn = (photo: Photo) => photo.type === PhotoType.LISTING_PHOTO;
        const mapFn = (photo: Photo) => photo.file;

        const photo = this.photos.filter(filterFn).map(mapFn);

        return photo;
    }
}
