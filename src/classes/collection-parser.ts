import { Collection } from '@interfaces/collection';

import { PageMetaParser } from '@classes/page-meta-parser';
import { UserMetaParser } from '@classes/user-meta-parser';

export class CollectionParser {
    public userMeta: UserMetaParser;
    public pageMeta: PageMetaParser;

    constructor(collection: Collection) {
        this.userMeta = new UserMetaParser(collection);
        this.pageMeta = new PageMetaParser(collection);
    }
}
