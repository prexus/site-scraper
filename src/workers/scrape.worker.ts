import '../alias';

import { errors as puppeteerErrors } from 'puppeteer';

import { expose } from 'threads/worker';

import { HarakiriError } from '@errors/harakiri';

import { Collection } from '@interfaces/collection';
import { Proxy } from '@interfaces/proxy';

import credentialsService from '@services/credentials.service';
import repositoryService from '@services/repository.service';
import utilityService from '@services/utility.service';
import loggerService from '@services/logger.service';
import fileService from '@services/file.service';

import screenshotHelper from '@helpers/screenshot.helper';
import extractHelper from '@helpers/extract.helper';
import profileHelper from '@helpers/profile.helper';
import packerHelper from '@helpers/packer.helper';
import metricsHelper from '@helpers/metrics.helper';
import metaHelper from '@helpers/meta.helper';
import pageHelper from '@helpers/page.helper';

let lastActivity = null;
let scrape = null;

async function start(workerName: string, proxy: Proxy) {
    loggerService.start(workerName);

    loggerService.info(`[SCRAPE WORKER] ${workerName} using proxy to ${proxy.destination}`);

    await pageHelper.start(proxy);

    try {
        // The harakiriPromise will reject (and ultimately throw)
        // in case no progress is reported from the scrape loop
        // for a set amount of time. This will trigger a restart
        const harakiriPromise = suicideWatch();
        const loopPromise = loop(workerName);

        await Promise.race([loopPromise, harakiriPromise]);
    } catch (error) {
        // The scrape loop threw an error so we will record it
        // but otherwise continue as if nothing happened. If
        // we just continue the browser closes and restarts.
        loggerService.error(`[SCRAPE WORKER] ${workerName} crashed with a ${error.name} saying '${error.message}' but will restart shortly`);

        await metricsHelper.failedScrapes(workerName, scrape.category);
    }

    // The scrape loop was interrupted so we will close
    // the browser and the function will return
    // triggering a new run from the worker service
    await pageHelper.stop();
}

async function loop(workerName: string) {
    let slowScrapes = 0;

    while (true) {
        registerActivity();

        const credentials = await credentialsService.getCredentials();

        if (!credentials) {
            loggerService.debug('[SCRAPE WORKER] No available credentials for scraping');

            await utilityService.sleep(60 * 15);

            continue;
        }

        scrape = await repositoryService.takeNextScrape();

        if (!scrape) {
            loggerService.debug('[SCRAPE WORKER] Nothing to scrape at the moment, waiting 5 seconds');

            await utilityService.sleep(5);

            continue;
        }

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Starting scrape on category [${scrape.category}]${scrape.retries ? ` - Retry attempt ${scrape.retries}` : ''}`);

        const listing = await fileService.load(scrape.id, 'listing.json');
        const listingUrl = listing.absolute_url.en;

        let success: boolean;

        try {
            success = await pageHelper.goto(listingUrl);
        } catch (error) {
            if (error instanceof puppeteerErrors.TimeoutError) {
                loggerService.warn(`[SCRAPE WORKER] ${credentials.email} could not resolve navigation in time`);

                return;
            }

            // Not a timeout...
            throw error;
        }

        if (!success) {
            loggerService.warn(`[SCRAPE WORKER] #${scrape.id} Scrape failed as server did not respond as expected`);

            await metricsHelper.failedScrapes(workerName, scrape.category);
            await repositoryService.failScrape(scrape.id);

            continue;
        }

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Taking screenshot`);
        const screenshot = await screenshotHelper.getScreenshot();

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Getting source`);
        const source = await pageHelper.getContent();

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Extracting entities`);
        const extract = await extractHelper.getExtract(scrape.id, listing, source);

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Extracting profile`);
        const profile = await profileHelper.getProfile(listing, credentials);

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Scraping completed successfully`);
        const completedScrape = await repositoryService.completeScrape(scrape.id);

        // Collect data
        const collection: Collection = {
            listing,
            screenshot,
            source,
            extract,
            profile
        };

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Collecting meta data`);
        const data = metaHelper.meta(completedScrape, collection);

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Packing scrape`);
        const exportPath = await packerHelper.packer(scrape, data);

        loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Scrape exported to ${exportPath}`);
        await metricsHelper.completedScrapes(workerName, scrape.category);

        // Returns seconds since last activity and helps us check how
        // long our scrapes take while keeping suicideWatch at bay
        const duration = registerActivity();

        if (duration > 120) {
            slowScrapes++;

            loggerService.warn(`[SCRAPE WORKER] #${scrape.id} Scrape took ${duration} seconds to complete`);
        } else {
            loggerService.debug(`[SCRAPE WORKER] #${scrape.id} Scrape took ${duration} seconds to complete`);

            if (slowScrapes > 0) {
                slowScrapes--;
            }
        }

        if (slowScrapes > 3) {
            loggerService.warn(`[SCRAPE WORKER] #${scrape.id} Current proxy had ${slowScrapes} slow scrapes within a short period of time.`);

            return;
        }
    }
}

function registerActivity() {
    const now = new Date().getTime();
    const msSinceLast = lastActivity ? now - lastActivity : 0;
    const secondsSinceLast = Math.round(msSinceLast / 1000);

    lastActivity = now;

    return secondsSinceLast;
}

async function suicideWatch() {
    registerActivity();

    while (true) {
        if (harakiri()) {
            throw new HarakiriError(`No progress reported for ${getTimeSinceLastActivity()} minutes`);
        }

        await utilityService.sleep(5);
    }
}

function getTimeSinceLastActivity() {
    const now = new Date().getTime();
    const ms = now - lastActivity;
    const seconds = ms / 1000;
    const minutes = Math.round(seconds / 60);

    return minutes;
}

function harakiri() {
    const maxIdleMinutes = 5;
    const minutes = getTimeSinceLastActivity();

    return minutes > maxIdleMinutes;
}

expose(start);
