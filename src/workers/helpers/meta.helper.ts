import { UserMeta, PageMeta, FilesMeta, ScrapeMeta, Meta } from '@interfaces/meta';
import { Collection } from '@interfaces/collection';
import { Scrape } from '@interfaces/scrape';

import { CollectionParser } from '@classes/collection-parser';
import { UserMetaParser } from '@classes/user-meta-parser';
import { PageMetaParser } from '@classes/page-meta-parser';

const helper = {
    meta
};

function meta(scrape: Scrape, collection: Collection): Collection {
    const parser = new CollectionParser(collection);

    const metaDocument: Meta = {
        scrape: getScrapeMeta(scrape),
        user: getUserMeta(parser.userMeta),
        page: getPageMeta(parser.pageMeta),
        files: getFilesMeta(collection)
    };

    return {
        meta: metaDocument,
        ...collection
    };
}

function getScrapeMeta(scrape: Scrape): ScrapeMeta {
    return {
        version: '1.0.0',
        created: scrape.created,
        started: scrape.started,
        completed: scrape.completed,
        category: scrape.category,
        identifier: scrape.identifier
    };
}

function getUserMeta(user: UserMetaParser) {
    const userMeta: Partial<UserMeta> = {};

    userMeta.id = user.getId();
    userMeta.companyId = user.getCompanyId();
    userMeta.created = user.getCreated();
    userMeta.name = user.getName();
    userMeta.company = user.getCompany();
    userMeta.photo = user.getProfilePhoto();
    userMeta.phoneNumber = user.getPrimaryPhoneNumber();
    userMeta.otherPhoneNumbers = user.getOtherPhoneNumber();

    return userMeta;
}

function getPageMeta(page: PageMetaParser) {
    const pageMeta: Partial<PageMeta> = {};

    pageMeta.created = page.getCreated();
    pageMeta.added = page.getAdded();
    pageMeta.url = page.getUrl();
    pageMeta.headline = page.getHeadline();
    pageMeta.content = page.getContent();
    pageMeta.photos = page.getPhotos();

    return pageMeta;
}

function getFilesMeta(collection: Collection) {
    const filesMeta: Partial<FilesMeta> = {};

    const photos = collection.extract.photos;

    // Specific types
    filesMeta.meta = 'meta.json';
    filesMeta.source = collection.source ? 'source.html' : null;
    filesMeta.screenshot = collection.screenshot ? 'screenshot.png' : null;

    // Generic additions that could be different
    // depending on the site that is scraped but
    // now we only support one, so...
    filesMeta.listing = collection.listing ? 'listing.json' : null;
    filesMeta.profile = collection.profile ? 'profile.json' : null;
    filesMeta.extract = collection.extract ? 'extract.json' : null;
    filesMeta.photos = photos.map((photo) => photo.file);

    return filesMeta;
}

export default helper;
