import repositoryService from '@services/repository.service';

const metrics = {
    // Captcha metrics
    detectedCaptchas,
    solvedCaptchas,
    failedCaptchas,

    // Listing metrics
    addedListings,

    // Scrape metrics
    failedScrapes,
    completedScrapes
};

async function detectedCaptchas() {
    await repositoryService.addMetric('detectedCaptchas', 'inc');
}

async function solvedCaptchas() {
    await repositoryService.addMetric('solvedCaptchas', 'inc');
}

async function failedCaptchas() {
    await repositoryService.addMetric('failedCaptchas', 'inc');
}

async function addedListings(category: string, count: number) {
    await repositoryService.addMetric('addedListings', 'inc', [{ category }, count]);
}

async function failedScrapes(worker: string, category: string) {
    await repositoryService.addMetric('failedScrapes', 'inc', { worker, category });
}

async function completedScrapes(worker: string, category: string) {
    await repositoryService.addMetric('completedScrapes', 'inc', { worker, category });
}

export default metrics;
