import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';

import apiDefinitions from '@definitions/api.definitions';

import { Category } from '@enums/category';

import { Listing, ListingResponse } from '@dtos/listing';

import { SiteScraperAPI } from '@interfaces/site-scraper-api';

import loggerService from '@services/logger.service';
import utilityService from '@services/utility.service';
import settingsService from '@services/settings.service';

puppeteer.use(StealthPlugin());

const helper = {
    getListings
};

type ListingsCallback = (category: Category, listings: Listing[]) => Promise<boolean>;

async function getListings(category: Category, listingsCallback: ListingsCallback) {
    const apiData = apiDefinitions[category];
    let apiValid = isApiValid(apiData);

    if (!apiValid) {
        apiValid = await renewApiData(apiData);
    }

    if (!apiValid) {
        return;
    }

    const url = getUrl(apiData);
    const method = 'POST';
    const headers = getHeaders();

    const settings = settingsService.getWorkerSettings();
    const maxPage = settings.listingWorkerMaxPage;

    let currentPage = 1;
    let totalPages = 1;
    let fetchMore = true;

    while (fetchMore && currentPage <= totalPages) {
        loggerService.debug(`[LISTING HELPER] ${category} Fetching data for page ${currentPage}`);

        const body = getPayload(apiData, currentPage);
        const options = { headers, body, method };

        try {
            const response = await fetch(url, options);
            const responseText = await response.text();

            const listingResponse = utilityService.json<ListingResponse>(responseText);
            const listingResult = listingResponse?.results?.pop();

            if (response.status !== 200) {
                loggerService.error(`[LISTING HELPER] ${category} Received status ${response.status}:`, responseText);

                break;
            }

            if (!listingResponse) {
                loggerService.error(`[LISTING HELPER] ${category} Received unparsable listing response:`, responseText);

                break;
            }

            if (!listingResult) {
                loggerService.error(`[LISTING HELPER] ${category} Received no results property:`, listingResponse);

                break;
            }

            const listings = listingResult.hits;

            fetchMore = await listingsCallback(category, listings);
            totalPages = listingResult.nbPages;

            loggerService.debug(`[LISTING HELPER] ${category} Data completed for ${currentPage}/${fetchMore ? maxPage ?? totalPages : currentPage}`);

            currentPage++;
        } catch (error) {
            loggerService.error(`[LISTING HELPER] ${category} Received unexpected response`, error);

            fetchMore = false;
        }

        if (maxPage && currentPage > maxPage) {
            fetchMore = false;
        }

        await utilityService.sleep(3, 8);
    }
}

function getUrl(apiData: SiteScraperAPI) {
    const querystring = getQueryString(apiData);
    const appId = apiData.apiAppId.toLowerCase();
    const url = `https://${appId}-dsn.algolia.net/1/indexes/*/queries?${querystring}`;

    return url;
}

function getHeaders() {
    return {
        accept: '*/*',
        'accept-language': 'da-DK,da;q=0.9',
        'content-type': 'application/x-www-form-urlencoded',
        'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': 'Windows',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'cross-site',
        Referer: 'https://site-scraper.com/',
        'Referrer-Policy': 'origin-when-cross-origin'
    };
}

function getQueryString(apiData: SiteScraperAPI) {
    const queryString = new URLSearchParams({
        'x-algolia-agent': 'Algolia%20for%20JavaScript%20(4.11.0)%3B%20Browser%20(lite)',
        'x-algolia-api-key': apiData.apiKey,
        'x-algolia-application-id': apiData.apiAppId
    });

    return queryString.toString();
}

function getPayload(apiData: SiteScraperAPI, page = 1, hitsPerPage = 25) {
    const params = new URLSearchParams();

    params.append('page', page.toString());
    params.append('hitsPerPage', hitsPerPage.toString());
    params.append('filters', apiData.filters);
    params.append('attributesToHighlight', '[]');
    params.append('attributesToRetrieve', JSON.stringify(apiData.attributes));

    if (apiData.facets) {
        params.append('facets', JSON.stringify(apiData.facets));
    }

    const request = {
        indexName: apiData.index,
        query: '',
        params: params.toString()
    };

    const payload = {
        requests: [request]
    };

    return JSON.stringify(payload);
}

async function renewApiData(api: SiteScraperAPI) {
    loggerService.debug(`[LISTING HELPER] ${api.type} Getting a new API key`);

    const data = await getAlgoliaData(api);

    if (!data) {
        loggerService.error(`[LISTING HELPER] ${api.type} Unable to extract API key`);

        return false;
    }

    api.apiExpiry = data.validUntil;
    api.apiAppId = data.appId;
    api.apiKey = data.apiKey;

    loggerService.debug(`[LISTING HELPER] ${api.type} Registered API key successfully`);

    return true;
}

function isApiValid(api: SiteScraperAPI) {
    if (!api.apiKey) {
        return false;
    }

    const now = new Date();
    const buffer = 1000 * 60 * 10; // 10 minutes
    const expiry = new Date(now.getTime() - buffer);
    const expired = api.apiExpiry > expiry;

    return expired;
}

async function getAlgoliaData(api: SiteScraperAPI) {
    // Known API source data
    const regList = [
        /algoliasearch\('(?<appId>.+?)'[, ]+'(?<apiKey>.+?)'/,
        /\\"appId\\":\\"(?<appId>.+?)\\",\\"apiKey\\":\\"(?<apiKey>.+?)\\",/
    ];

    const generalOptions = settingsService.getPuppeteerSettings();
    const customOptions = { headless: true, args: [] };
    const options = { ...generalOptions, ...customOptions };

    const browser = await puppeteer.launch(options);
    const page = await browser.newPage();

    let algolia = null;

    try {
        await page.goto(api.apiSource);

        const html = await page.content();

        for (const regex of regList) {
            const test = new RegExp(regex);
            const match = test.exec(html);

            if (match) {
                const data = match.groups;

                algolia = {
                    apiKey: data.apiKey,
                    appId: data.appId,
                    validUntil: extractValidity(data.apiKey)
                };

                break;
            }
        }
    } catch (error) {
        loggerService.error(`[LISTING HELPER] ${api.type} Unable to navigate when fetching API key`, error);
    }

    await page.close();
    await browser.close();

    return algolia;
}

// The API key is a base64 encoded string including a key,
// user token and expiry. The key looks something like this:
//
//    79fe52588140bbbee869f888636ce2642380f16d9ba379f
//    01319683f60c38c5duserToken=3qi031foxlm5o5diqw91
//    412an8bwcc6p&validUntil=1666083600
//
// This function extracts the validity and converts it to
// miliseconds so it is comparable to javascipts Date().
function extractValidity(apiKey: string): Date {
    const plain = Buffer.from(apiKey, 'base64').toString();
    const validityRegexp = /validUntil=(?<validUntil>\d{10})/;
    const validityResult = validityRegexp.exec(plain);
    const validUntil = validityResult?.groups.validUntil;

    return Number.isNaN(validUntil) ? null : new Date(Number(validUntil) * 1000);
}

export default helper;
