import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import { Browser, Page } from 'puppeteer';

import { Proxy } from '@interfaces/proxy';

import settingsService from '@services/settings.service';
import loggerService from '@services/logger.service';
import pageService from '@services/page.service';

puppeteer.use(StealthPlugin());

const helper = {
    start,
    stop,
    goto,
    getPage,
    getContent
};

let browser: Browser = null;
let page: Page = null;

async function start(proxy: Proxy) {
    if (page || browser) {
        throw new Error('Browser is already open.');
    }

    const generalOptions = settingsService.getPuppeteerSettings(proxy);
    const customOptions = { defaultViewport: { width: 1280, height: 1024 } };
    const options = { ...generalOptions, ...customOptions };

    browser = await puppeteer.launch(options);
    [page] = await browser.pages();

    const username = proxy.username;
    const password = proxy.password;

    await page.authenticate({ username, password });

    browser.on('targetcreated', async (target) => {
        if (target.type === 'page') {
            loggerService.warn(`[PAGE HELPER] Closed stray tab going to ${target.url()}!`);

            await target.close();
        }
    });
}

async function stop() {
    await browser.close();

    browser = null;
    page = null;
}

async function goto(url: string) {
    // High timeout as its important that we finish running. If it's continously
    // slow, then we'll switch proxy instead
    const response = await page.goto(url, { timeout: 120000 });
    const success = await pageService.checkReCAPTCHA(page);
    const status = response.status();

    if (!success) {
        return false;
    }

    if (status !== 200) {
        return false;
    }

    await pageService.waitForHtmlRender(page);

    return true;
}

function getPage() {
    return page;
}

function getContent() {
    return page.content();
}

export default helper;
