import { Collection } from '@interfaces/collection';
import { Scrape } from '@interfaces/scrape';

import fileService from '@services/file.service';

const helper = {
    packer
};

async function packer(scrape: Scrape, data: Collection) {
    await fileService.save(scrape.id, 'screenshot.png', data.screenshot);
    await fileService.save(scrape.id, 'source.html', data.source);
    await fileService.save(scrape.id, 'extract.json', data.extract);
    await fileService.save(scrape.id, 'profile.json', data.profile);
    await fileService.save(scrape.id, 'meta.json', data.meta);

    const exportPath = await fileService.compress(scrape);

    await fileService.clean(scrape.id);

    return exportPath;
}

export default helper;
