import jwt_decode, { JwtPayload } from 'jwt-decode';

import credentialsService from '@services/credentials.service';
import utilityService from '@services/utility.service';
import loggerService from '@services/logger.service';
import pageService from '@services/page.service';

import { BotDetectionError } from '@errors/bot-detection';

import { ProfileReponse } from '@dtos/profile';
import { Listing } from '@dtos/listing';
import { LoginTokenResponse, RefreshTokenResponse, TokenResponse } from '@dtos/login';

import { Credentials } from '@interfaces/credentials';

import pageHelper from '@helpers/page.helper';

const helper = {
    getProfile
};

async function getProfile(listing: Listing, credentials: Credentials) {
    const categoryId = listing.category_id;
    const uuid = listing.uuid;

    if (!categoryId || !uuid) {
        return null;
    }

    loggerService.debug(`[PROFILE HELPER] Fetching profile on ${categoryId}/${uuid}`);

    const profileUrl = `https://site-scraper.com/api/v4/leads/${categoryId}/${uuid}/listing-profile/`;

    const validated = await validateAccessTokens(credentials);

    if (!validated) {
        return null;
    }

    const profile = await makeProfileRequest(credentials, profileUrl);

    loggerService.debug(`[PROFILE HELPER] Profile name: ${profile?.full_name || 'N/A'}, number: ${profile?.phone_number || 'N/A'}`);

    return profile;
}

async function validateAccessTokens(credentials: Credentials) {
    let status = getTokenStatus(credentials);

    let newTokens: TokenResponse = null;

    if (status === 'refresh') {
        newTokens = await refresh(credentials);
    }

    if (status === 'login' || (status === 'refresh' && !newTokens)) {
        newTokens = await login(credentials);
    }

    if (newTokens) {
        credentials.accessToken = newTokens.access_token;
        credentials.refreshToken = newTokens.refresh_token;

        status = 'ok';
    }

    return (status === 'ok');
}

function getTokenStatus(credentials: Credentials) {
    if (!credentials.accessToken || !credentials.refreshToken) {
        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] No tokens available, login required`);

        return 'login';
    }

    const now = new Date().getTime() / 1000;

    const decodedAccessToken: JwtPayload = jwt_decode(credentials.accessToken);
    const decodedRefreshToken: JwtPayload = jwt_decode(credentials.refreshToken);

    const accessTokenExpiresInSeconds = decodedAccessToken.exp - now;
    const refreshTokenExpiresInSeconds = decodedRefreshToken.exp - now;

    // Remaining validity more than 30 seconds
    if (accessTokenExpiresInSeconds > 30) {
        const timeRemaining = utilityService.timeRemaining(accessTokenExpiresInSeconds);

        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Access token valid for another ${timeRemaining}`);

        return 'ok';
    }

    // Access token has expired, but refresh token is valid
    // and we can just get new tokens to use.
    if (refreshTokenExpiresInSeconds > 60) {
        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Access token expired, using refresh token to renew`);

        return 'refresh';
    }

    loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Refresh token expired, login again`);

    return 'login';
}

async function login(credentials: Credentials) {
    const loginFn = async (inCredentials: Credentials) => {
        const data = new FormData();

        data.append('username', inCredentials.email);
        data.append('password', inCredentials.password);

        const loginResponse = await fetch('https://site-scraper.com/en/auth/login/', {
            method: 'POST',
            body: data
        });

        const status = loginResponse.status;
        const text = await loginResponse.text();

        return { status, text };
    };

    const page = pageHelper.getPage();
    const data = await page.evaluate(loginFn, credentials);

    if (data.status === 403) {
        loggerService.error(`[PROFILE HELPER] ${credentials.email} has been banned! Response was: '${data.text.substring(0, 25)}...'`);

        await credentialsService.markAsBanned(credentials);

        const newCredentials = await credentialsService.getCredentials();

        if (!newCredentials) {
            throw Error('Out of credentials...');
        }

        // We will just try again with a new set of credentials
        // and hopefully the next one will work just fine!
        return login(newCredentials);
    }

    const loginTokens = utilityService.json<LoginTokenResponse>(data.text);

    if (loginTokens) {
        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Successfully logged in`);
    } else {
        loggerService.error(`[PROFILE HELPER] [${credentials.email}] Failed logging in!`);
    }

    return loginTokens;
}

async function refresh(credentials: Credentials) {
    const refreshFn = async (inCredentials: Credentials) => {
        const response = await fetch('https://site-scraper.com/en/auth/refresh_token/', {
            headers: {
                'x-access-token': inCredentials.accessToken,
                'x-refresh-token': inCredentials.refreshToken
            },
            method: 'POST'
        });

        return response.text();
    };

    const page = pageHelper.getPage();
    const refreshText = await page.evaluate(refreshFn, credentials);
    const refreshTokens = utilityService.json<RefreshTokenResponse>(refreshText);

    if (refreshTokens) {
        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Tokens successfully refreshed`);
    } else {
        loggerService.error(`[PROFILE HELPER] [${credentials.email}] Failed refreshing token. Please login again`);
    }

    return refreshTokens;
}

async function makeProfileRequest(credentials: Credentials, profileUrl: string) {
    const refreshFn = async (inProfileUrl: string, inAccessToken: string) => {
        const response = await fetch(inProfileUrl, {
            headers: {
                'x-access-token': inAccessToken
            },
            method: 'GET'
        });

        return {
            status: response.status,
            text: await response.text()
        };
    };

    const page = pageHelper.getPage();
    const response = await page.evaluate(refreshFn, profileUrl, credentials.accessToken);

    // This happens on some profiles only and I cannot seem
    // to figure out why. It's not detectable from any of the
    // data that we see on the client, so we just have to try
    // and treat the 403 as 'oh well, there is no profile data'
    if (response.status === 403) {
        loggerService.verbose(`[PROFILE HELPER] [${credentials.email}] Access denied trying to fetch profile`);

        return null;
    }

    if (response.status !== 200) {
        loggerService.error(`[PROFILE HELPER] [${credentials.email}] Fetching profile responded with ${response.status}!`);

        return null;
    }

    const profileData = utilityService.json<ProfileReponse>(response.text);

    if (!profileData) {
        const isBotDetected = pageService.botDetected(response.text);

        if (isBotDetected) {
            throw new BotDetectionError(`Detection triggered for ${credentials.email}`);
        }

        loggerService.error(`[PROFILE HELPER] [${credentials.email}] Unable to parse profile!`);
        loggerService.error(`[PROFILE HELPER] ${response.text.substring(0, 50)}`);
    }

    return profileData;
}

export default helper;
