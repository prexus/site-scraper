import { findPhoneNumbersInText } from 'libphonenumber-js';
import { JSDOM } from 'jsdom';

import ocrService from '@services/ocr.service';
import fileService from '@services/file.service';
import loggerService from '@services/logger.service';
import utilityService from '@services/utility.service';

import { PhotoType } from '@enums/photo';

import { Listing } from '@dtos/listing';
import { Context } from '@dtos/context';

import { Photo } from '@interfaces/photo';
import { Extract } from '@interfaces/collection';

const helper = {
    getExtract
};

async function getExtract(scrapeId: number, listing: Listing, html: string): Promise<Extract> {
    const photos: Photo[] = [];

    // *******************
    // ** PROFILE PHOTO **
    // *******************

    const lister = extractLister(html);

    const profilePhotoUrl = lister?.photo_url ?? listing.agent?.logo ?? null;

    if (profilePhotoUrl) {
        const fileNameNoExt = `photo-${photos.length + 1}`;
        const downloadedFile = await fileService.download(scrapeId, profilePhotoUrl, fileNameNoExt);

        photos.push({
            type: PhotoType.PROFILE_PHOTO,
            url: profilePhotoUrl,
            file: downloadedFile
        });
    }

    // *************
    // ** Context **
    // *************

    const context = extractContext(html);

    // ************
    // ** PHOTOS **
    // ************
    const jsdom = new JSDOM(html);
    const document = jsdom.window.document;
    const elements = document.querySelectorAll('.image-gallery-image');

    for (const element of elements) {
        const photoUrl = element.getAttribute('src');
        const fileNameNoExt = `photo-${photos.length + 1}`;
        const downloadedFile = await fileService.download(scrapeId, photoUrl, fileNameNoExt);

        photos.push({
            type: PhotoType.LISTING_PHOTO,
            url: photoUrl,
            file: downloadedFile
        });
    }

    // *************
    // ** PHONENO **
    // *************
    const description = document.querySelector('[class*="DescriptionText"] span')?.innerHTML || document.querySelector('h5 + div[type="large"]')?.innerHTML || '';
    const allDescriptions = [
        description,
        listing.description?.en,
        listing.description?.ar
    ];

    const primaryPhoneNumber = await extractPrimaryPhoneNumber(document);
    const inlinePhoneNumbers = findPhoneNumbers(allDescriptions);

    loggerService.debug(`[EXTRACT HELPER] Extracted ${photos.length} photos and ${inlinePhoneNumbers.length + (primaryPhoneNumber ? 1 : 0)} phone numbers`);

    return {
        description,
        context,
        lister,
        photos,
        primaryPhoneNumber,
        inlinePhoneNumbers
    };
}

async function extractPrimaryPhoneNumber(document: Document): Promise<string> {
    let identifiedPhoneNumber: string = null;

    const phoneDiv = document.querySelector('div[data-fnid="agent-number"]')?.textContent;
    const phoneImg = document.querySelector('div[data-fnid="agent-number"] img')?.getAttribute('src');

    if (phoneImg) {
        // data/png is available and we need to OCR it
        identifiedPhoneNumber = await ocrService.extractPhoneNo(phoneImg);
    } else if (phoneDiv) {
        // We managed to capture the actual number
        identifiedPhoneNumber = phoneDiv;
    }

    return identifiedPhoneNumber;
}

function extractLister(html: string) {
    const listerPattern = '"lister":{(?<lister>.*?)}';
    const listerRegex = new RegExp(listerPattern);
    const listerMatch = listerRegex.exec(html);
    const listerString = listerMatch?.groups.lister;
    const lister = listerString ? JSON.parse(`{ ${listerString} }`) : null;

    return lister;
}

function extractContext(html: string) {
    const contextPattern = 'window.__INITIAL_STATE__ = JSON.parse\\("(?<context>.*?)"\\);\\n';
    const contextRegex = new RegExp(contextPattern);
    const contextMatch = contextRegex.exec(html);
    const contextString = contextMatch?.groups.context;
    const context = utilityService.jsonEval<Context>(contextString);

    if (context) {
        // Clean it up as it is stupidly big
        const taxonomyData = context.taxonomy?.data;
        const bytesTotal = taxonomyData ? JSON.stringify(taxonomyData).length : 0;

        if (bytesTotal > 0) {
            const bytesSaved = utilityService.toByteSize(bytesTotal);

            context.taxonomy.data = `// Redacted ${bytesSaved} of data`;
        }
    }

    return context;
}

function findPhoneNumbers(texts: string[]): string[] {
    const validTexts = texts.filter(Boolean);
    const allPhoneNumbers = [];

    for (const text of validTexts) {
        const extractedNumbers = findPhoneNumbersInText(text, 'AE');
        const phoneNumbersE164 = extractedNumbers.map((entry) => entry.number.number);

        allPhoneNumbers.push(...phoneNumbersE164);
    }

    const uniquePhoneNumbers = new Set(allPhoneNumbers);

    return [...uniquePhoneNumbers];
}

export default helper;
