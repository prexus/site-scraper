import pageService from '@services/page.service';

import pageHelper from '@helpers/page.helper';

const helper = {
    getScreenshot
};

async function getScreenshot() {
    const page = pageHelper.getPage();

    // Prepares the page by clicking buttons that
    // reveals more information about the listing
    await pageService.expandDescription(page);
    await pageService.expandMap(page);

    await pageService.checkForAgentButton(page);
    await pageService.checkForPhoneNumberButtons(page);

    await pageService.clean(page);

    // Using our own network idle check as it doesnt
    // throw, it just continues. Map tiles takes a
    // little while to load.
    await pageService.waitForNetworkIdle(page);

    // Little circumvension as the screenshot function
    // returns both string and buffer depending on what
    // you feed the function.
    const intermediary = page.screenshot({ fullPage: true }) as unknown;
    const buffer = intermediary as Buffer;

    return buffer;
}

export default helper;
