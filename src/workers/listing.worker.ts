import '../alias';

import { expose } from 'threads/worker';

import { Category } from '@enums/category';

import { Listing } from '@interfaces/dtos/listing';

import fileService from '@services/file.service';
import loggerService from '@services/logger.service';
import utilityService from '@services/utility.service';
import settingsService from '@services/settings.service';
import repositoryService from '@services/repository.service';

import metricsHelper from '@helpers/metrics.helper';
import listingHelper from '@helpers/listing.helper';

loggerService.start();

let rescanAllListings: Boolean;
let sessionIdentifiers: string[];

async function start() {
    const settings = settingsService.getWorkerSettings();
    rescanAllListings = settings.listingWorkerRescan;

    const categories = Object.keys(Category);

    while (true) {
        for (const value of categories) {
            sessionIdentifiers = [];
            const category: Category = Category[value];

            loggerService.verbose(`[LISTING WORKER] ${category} Fetching new listings`);
            await listingHelper.getListings(category, onListingsReceived);
        }

        // If rescanning was enabled we are done now and
        // want to disable it so it's not rescanning again
        rescanAllListings = false;

        await utilityService.sleep(30, 120);
    }
}

async function onListingsReceived(category: Category, listings: Listing[]) {
    loggerService.verbose(`[LISTING WORKER] ${category} Received ${listings.length} listings`);
    const fetchMore = await saveListings(category, listings);

    return fetchMore;
}

async function saveListings(category: Category, listings: Listing[]) {
    let fetchMore = true;
    let newEntries = 0;

    for (const listing of listings) {
        // Consider hashing this maybe? It is also used for the
        // filename of the exported .scrape file
        const identifier = `${listing.added}-${listing.id}`;

        const haveSeenIdentifier = sessionIdentifiers.includes(identifier);

        if (haveSeenIdentifier) {
            // This identifier is already recorded in this session.
            // It has probably been moved from previous page as new
            // item came in.
            loggerService.verbose(`[LISTING WORKER] ${category} Already seen ${identifier} in this session - skipping`);

            continue;
        }

        sessionIdentifiers.push(identifier);

        const exists = await repositoryService.hasScraped(category, identifier);

        if (exists && !rescanAllListings) {
            fetchMore = false;

            break;
        }

        if (!exists) {
            const scrapeId = await repositoryService.createScrape(category, identifier);

            await fileService.save(scrapeId, 'listing.json', listing);

            newEntries++;
        }
    }

    loggerService.verbose(`[LISTING WORKER] ${category} Added ${newEntries} new scrape entries`);

    await metricsHelper.addedListings(category, newEntries);

    return fetchMore;
}

expose(start);
