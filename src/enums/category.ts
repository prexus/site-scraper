export enum Category {
    MOTORS = 'motors',
    CLASSIFIED = 'classified',
    COMMUNITY = 'community',
    PROPERTY_FOR_RENT_RESIDENTIAL = 'propertyForRentResidential'
}
