export enum Color {
    BLACK = '\x1b[30m',
    BLUE = '\x1b[34m',
    CYAN = '\x1b[36m',
    GREEN = '\x1b[32m',
    MAGENTA = '\x1b[35m',
    RED = '\x1b[31m',
    WHITE = '\x1b[37m',
    YELLOW = '\x1b[33m',
    RESET = '\x1b[0m'
}
